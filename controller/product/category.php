<?php
class ControllerProductCategory extends Controller {
	public function index() {
		$data['customer_group'] = $this->customer->getGroupId();
		$this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path . $url)
					);
				}
			}
		} else {
			$category_id = 0;
		}

		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info) {
			$this->document->setTitle($category_info['meta_title']);
			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path']), 'canonical');
			
			$data['filter_type'] = $category_info['filter_type'];

			$data['heading_title'] = $category_info['name'];

			$data['text_refine'] = $this->language->get('text_refine');
			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_price'] = $this->language->get('text_price');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_list'] = $this->language->get('button_list');
			$data['button_grid'] = $this->language->get('button_grid');

			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $category_info['name'],
				'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
			);

			if ($category_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$data['compare'] = $this->url->link('product/compare');

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['categories'] = array();

			$results = $this->model_catalog_category->getCategories($category_id);

			foreach ($results as $result) {
				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);

				$data['categories'][] = array(
					'name'  => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
				);
			}

			$data['products'] = array();

			$filter_1 = '';
			$filter_2 = '';
			$filter_3 = '';
			$filter_4 = '';
			$filter_5 = '';
			$filter_6 = '';
			$filter_options = '';
			if (isset($this->request->get['filter_mark'])) $filter_1 = $this->request->get['filter_mark'];
			if (isset($this->request->get['filter_model'])) $filter_2 = $this->request->get['filter_model'];
			if (isset($this->request->get['filter_ym'])) $filter_3 = $this->request->get['filter_ym'];
			if (isset($this->request->get['filter_dvigatel'])) $filter_4 = $this->request->get['filter_dvigatel'];
			if (isset($this->request->get['filter_rul'])) $filter_5 = $this->request->get['filter_rul'];
			if (isset($this->request->get['filter_raspologenie'])) $filter_6 = $this->request->get['filter_raspologenie'];
			if (isset($this->request->get['filter_options'])) $filter_options = $this->request->get['filter_options'];
			$data['filter_1'] = $filter_1;
			$data['filter_2'] = $filter_2;
			$data['filter_3'] = $filter_3;
			$data['filter_4'] = $filter_4;
			$data['filter_5'] = $filter_5;
			$data['filter_6'] = $filter_6;
			$data['filter_options'] = $filter_options;
			
			$filter_data = array(
				'filter_category_id' => $category_id,
				'filter_1'	=>	$filter_1,
				'filter_2'	=>	$filter_2,
				'filter_3'	=>	$filter_3,
				'filter_4'	=>	$filter_4,
				'filter_5'	=>	$filter_5,
				'filter_6'	=>	$filter_6,
				'filter_options'	=>	$filter_options,
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);
			
			$filter_mark = $this->model_catalog_product->getFilterMark($filter_data);
			
			$filter_model = array();
			if ($filter_1)
			{
				$filter_model = $this->model_catalog_product->getFilterModel($filter_data);
			}
			$filter_ym = array();
			if ($filter_2)
			{
				$filter_ym_temp = $this->model_catalog_product->getFilterYm($filter_data);
				foreach ($filter_ym_temp as $one_element)
				{
					if ($one_element['image']) {
						$image = $this->model_tool_image->resize($one_element['image'], 200, 150);
					} else {
						$image = $this->model_tool_image->resize('no_image.jpg', 200, 150);
					}
					
					if ($one_element['image']) {
						$image_mini = $this->model_tool_image->resize($one_element['image'], 32, 22);
					} else {
						$image_mini = $this->model_tool_image->resize('no_image.jpg', 32, 22);
					}
				
					$filter_ym[] = array(
						'filter_3' => $one_element['filter_3'],
						'image'	=>	$image,
						'image_mini'	=>	$image_mini
					);
				}
			}
			$data['image_filter_3'] = '';
			if ($filter_ym)
			{
				foreach ($filter_ym as $filter_ym_elelement) {
					if ($filter_ym_elelement['filter_3']==$filter_3)
					{
						$data['image_filter_3'] = $filter_ym_elelement['image_mini'];
					}
				}
			}
			
			$filter_rul = array();
			if ($filter_3)
			{
				$filter_rul = $this->model_catalog_product->getFilterRul($filter_data);
			}
			$filter_options_view = array();
			$filters_array = array();
			$products_array = array();
			if ($filter_5||($filter_3&&!$filter_rul))
			{
				$filter_options_view = $this->model_catalog_product->getFilterOptions($filter_data);
				
				$i = 0;
				$filters_array = array();
				foreach ($filter_options_view as $one_element)
				{
					$filter_array = array();
					$filter_explode = explode(",",$one_element['filter_options']);
					foreach ($filter_explode as $filter_explode_element)
					{
						$filter_explode_razdel = explode("|",$filter_explode_element);
						if ($filter_explode_razdel[0]=='Опции')
						{
							$filter_array[$filter_explode_razdel[1]] = $filter_explode_razdel[2];
						}
					}
					$find = 0;
					foreach ($filters_array as $proverka_array)
					{
						if ($proverka_array==$filter_array)
							$find = 1;
					}
					
					if ($find == 0)
					{
						$filters_array[$i] = $filter_array;
						
						$options = '';
						$for_implode = array();
						foreach ($filter_array as $for_options_key=>$for_options_temp)
						{
							$for_implode[] = implode('|',array('Опции',$for_options_key,$for_options_temp));
						}
						$options = implode(',',$for_implode);
						
						if ($one_element['image']) {
							$image = $this->model_tool_image->resize($one_element['image'], 200, 150);
						} else {
							$image = $this->model_tool_image->resize('no_image.jpg', 200, 150);
						}
						$products_array[$i] = array(
							'name'	=>	$one_element['name'],
							'image'	=>	$image,
							'options'	=>	$options
						);
						$i++;
					}
				}
			}
			$filter_dvigatel = array();
			if ($filter_3)
			{
				$filter_dvigatel = $this->model_catalog_product->getFilterDvigatel($filter_data);
			}
			$filter_raspologenie = array();
			if ($filter_3)
			{
				$filter_raspologenie = $this->model_catalog_product->getFilterBokovoe($filter_data);
			}
			
			// Текущий шаг
			$current_step = 1;
			if ($data['filter_type']==0)
			{
			$current_step = 'showproducts';
			}
			if ($data['filter_type']!=0)
			{
				if ($data['filter_type']==1)
				{
					if ($filter_options) $current_step = 'showproducts';
					else if ($filter_3&&!$filter_rul) $current_step = 5;
					else if ($filter_5) $current_step = 5;
					else if ($filter_3) $current_step = 4;
					else if ($filter_2) $current_step = 3;
					else if ($filter_1) $current_step = 2;
					else $current_step = 1;
				}
				else if ($data['filter_type']==2||$data['filter_type']==6)
				{
					if ($filter_3) $current_step = 'showproducts';
					else if ($filter_2) $current_step = 3;
					else if ($filter_1) $current_step = 2;
					else $current_step = 1;
				}
				else if ($data['filter_type']==3)
				{
					if ($filter_6) $current_step = 'showproducts';
					else if ($filter_3) $current_step = 4;
					else if ($filter_2) $current_step = 3;
					else if ($filter_1) $current_step = 2;
					else $current_step = 1;
				}
				else if ($data['filter_type']==4||$data['filter_type']==5)
				{
					if ($filter_4) $current_step = 'showproducts';
					else if ($filter_3) $current_step = 4;
					else if ($filter_2) $current_step = 3;
					else if ($filter_1) $current_step = 2;
					else $current_step = 1;
				}
			}
			
			$data['current_step'] = $current_step;
			
			$data['filter_category_id'] = $category_id;
			
			$data['filter_mark'] = $filter_mark;
			$data['filter_model'] = $filter_model;
			$data['filter_ym'] = $filter_ym;
			$data['filter_rul'] = $filter_rul;
			$data['filter_raspologenie'] = $filter_raspologenie;
			$data['filter_dvigatel'] = $filter_dvigatel;
			$data['filter_options'] = $filter_options;
			$data['filters_array'] = $filters_array;
			$data['products_array'] = $products_array;
			
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
			
			$data['current_url'] = html_entity_decode($this->url->link('product/category', $url), ENT_QUOTES, 'UTF-8');
			
			$filter_products = array();
			$filter_products_temp = array();
			if ($current_step=='showproducts')
			{
				$filter_products_temp = $this->model_catalog_product->getFilteredProducts($filter_data);
			}
			if ($filter_products_temp)
			{
				foreach ($filter_products_temp as $one_product)
				{
					if ($one_product['image']) {
						$image = $this->model_tool_image->resize($one_product['image'], 200, 150);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', 200, 150);
					}

					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($one_product['price']);
					} else {
						$price = false;
					}

					if ((float)$one_product['special']) {
						$special = $this->currency->format($one_product['special']);
					} else {
						$special = false;
					}
					
					$filter_products[] = array(
						'product_id'  => $one_product['product_id'],
						'image'       => $image,
						'name'        => $one_product['name'],
						'filter_options'        => $one_product['filter_options'],
						'description' => utf8_substr(strip_tags(html_entity_decode($one_product['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'manufacturer'=> $one_product['manufacturer'],
						'link'        => $this->url->link('product/product', 'product_id=' . $one_product['product_id'] . $url)
					);
					
				}
			}
			
			$data['filter_products'] = $filter_products;
			
			
			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				$price_2 = $this->currency->format($this->tax->calculate($result['price_2'], $result['tax_class_id'], $this->config->get('config_tax')));
				$price_3 = $this->currency->format($this->tax->calculate($result['price_3'], $result['tax_class_id'], $this->config->get('config_tax')));

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				
				// Shop blik5
				$mini_size_w = 100;
				$mini_size_h = 100;
				$analogs = array();
				if ($result['main_count']>1)
				{
					$related_products = $this->model_catalog_product->getProductRelated($result['product_id']);
					foreach ($related_products as $related)
					{
						if ($related['image']) {
							$image_mini = $this->model_tool_image->resize($related['image'], $mini_size_w, $mini_size_h);
						} else {
							$image_mini = $this->model_tool_image->resize('placeholder.png', $mini_size_w, $mini_size_h);
						}

						if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
							$price_mini = $this->currency->format($this->tax->calculate($related['price'], $related['tax_class_id'], $this->config->get('config_tax')));
						} else {
							$price_mini = false;
						}
					
						$analogs[] = array(
							'product_id'	=>	$related['product_id'],
							'thumb'			=>	$image_mini,
							'name'        	=> 	$related['name'],
							'color'        	=> 	'',
							'manufacturer'  => 	$related['manufacturer'],
							'price'       	=> 	$price_mini,
							'href'        	=> 	$this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $related['product_id'] . $url)
						);
					}
				}
				// End Shop blik5

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'price_2'       => $price_2,
					'price_3'       => $price_3,
					'name'        => $result['name'],
					'filter_options'        => $result['filter_options'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'analogs'     => $analogs,
					'manufacturer'=> $result['manufacturer'],
					'color'		  => '',
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'] . $url)
				);
			}
for ($i=0; $i<count($data['products']); $i++) {

$sql = "SELECT s.name,s.name_short,stp.quantity,t.name as zone_name,s.address,s.phone,s.worktime FROM " . DB_PREFIX . "skladi s LEFT JOIN " . DB_PREFIX . "skladi_to_products stp ON s.skladi_id = stp.sklad_id LEFT JOIN " . DB_PREFIX . "town t ON t.town_id=s.town_id WHERE stp.product_id='".$data['products'][$i]['product_id']."'";

		
		if (isset($this->session->data['current_town'])&&$this->session->data['current_town']!='')
			$sql .= " AND t.name='".$this->session->data['current_town']."'";
		else
			$sql .= " AND false ";
			
		$query = $this->db->query($sql);
		
		$warehouses = $query->rows;

        $data['warehouses'][$i] = $warehouses; // большой вложенный массив со всеми складами ко всем товарам (отфильтрованным)

}
			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			if (isset($this->request->get['filter_mark'])) {
				$url .= '&filter_mark=' . $this->request->get['filter_mark'];
			}
			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . $this->request->get['filter_model'];
			}
			if (isset($this->request->get['filter_ym'])) {
				$url .= '&filter_ym=' . $this->request->get['filter_ym'];
			}
			if (isset($this->request->get['filter_rul'])) {
				$url .= '&filter_rul=' . $this->request->get['filter_rul'];
			}
			if (isset($this->request->get['filter_raspologenie'])) {
				$url .= '&filter_raspologenie=' . $this->request->get['filter_raspologenie'];
			}
			if (isset($this->request->get['filter_dvigatel'])) {
				$url .= '&filter_dvigatel=' . $this->request->get['filter_dvigatel'];
			}
			if (isset($this->request->get['filter_options'])) {
				$url .= '&filter_options=' . $this->request->get['filter_options'];
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
				);
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
			);

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['filter_mark'])) {
				$url .= '&filter_mark=' . $this->request->get['filter_mark'];
			}
			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . $this->request->get['filter_model'];
			}
			if (isset($this->request->get['filter_ym'])) {
				$url .= '&filter_ym=' . $this->request->get['filter_ym'];
			}
			if (isset($this->request->get['filter_rul'])) {
				$url .= '&filter_rul=' . $this->request->get['filter_rul'];
			}
			if (isset($this->request->get['filter_raspologenie'])) {
				$url .= '&filter_raspologenie=' . $this->request->get['filter_raspologenie'];
			}
			if (isset($this->request->get['filter_dvigatel'])) {
				$url .= '&filter_dvigatel=' . $this->request->get['filter_dvigatel'];
			}
			if (isset($this->request->get['filter_options'])) {
				$url .= '&filter_options=' . $this->request->get['filter_options'];
			}

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
			
			if (isset($this->request->get['filter_mark'])) {
				$url .= '&filter_mark=' . $this->request->get['filter_mark'];
			}
			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . $this->request->get['filter_model'];
			}
			if (isset($this->request->get['filter_ym'])) {
				$url .= '&filter_ym=' . $this->request->get['filter_ym'];
			}
			if (isset($this->request->get['filter_rul'])) {
				$url .= '&filter_rul=' . $this->request->get['filter_rul'];
			}
			if (isset($this->request->get['filter_raspologenie'])) {
				$url .= '&filter_raspologenie=' . $this->request->get['filter_raspologenie'];
			}
			if (isset($this->request->get['filter_dvigatel'])) {
				$url .= '&filter_dvigatel=' . $this->request->get['filter_dvigatel'];
			}
			if (isset($this->request->get['filter_options'])) {
				$url .= '&filter_options=' . $this->request->get['filter_options'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/category.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/product/category.tpl', $data));
			}
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
	
	public function getfiltermodel() {
		$this->load->model('catalog/product');
		
		$filter_1 = '';
		$filter_2 = '';
		$filter_3 = '';
		if (isset($this->request->get['filter_mark'])) $filter_1 = $this->request->get['filter_mark'];
		$category_id = $this->request->get['category_id'];
		
		$filter_data = array(
			'filter_category_id' => $category_id,
			'filter_1'	=>	$filter_1,
			'filter_2'	=>	$filter_2,
			'filter_3'	=>	$filter_3
		);
		
		$filter_model = array();
		if ($filter_1!='')
		{
			$filter_model = $this->model_catalog_product->getFilterModel($filter_data);
		}
		
		$html = '';
		if ($filter_model)
		{
			$html .= '<select name="formmodel" class="form-control" filter_mark="'.$filter_1.'" >';
			$html .= '	<option value=""></option>';
			foreach ($filter_model as $one_element)
			{
			$html .= '	<option value="'.$one_element['filter_2'].'">'.$one_element['filter_2'].'</option>';
			}
			$html .= '</select>';
		}
		echo $html;
	}
	
	public function getfilterym() {
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
		$filter_1 = '';
		$filter_2 = '';
		$filter_3 = '';
		if (isset($this->request->get['filter_mark'])) $filter_1 = $this->request->get['filter_mark'];
		if (isset($this->request->get['filter_model'])) $filter_2 = $this->request->get['filter_model'];
		$category_id = $this->request->get['category_id'];
		
		$filter_data = array(
			'filter_category_id' => $category_id,
			'filter_1'	=>	$filter_1,
			'filter_2'	=>	$filter_2,
			'filter_3'	=>	$filter_3
		);
		
		$filter_ym = array();
		if ($filter_2!='')
		{
			$filter_ym = $this->model_catalog_product->getFilterYm($filter_data);
		}
		
		$html = '';
		if ($filter_ym)
		{
			$html .= '<select name="formym" filter_mark="'.$filter_1.'" filter_model="'.$filter_2.'" class="form-control">';
			$html .= '	<option value=""></option>';
			$i = 0;
			foreach ($filter_ym as $one_element)
			{
			$html .= '	<option value="'.$one_element['filter_3'].'" id="option_id_'.$i.'">'.$one_element['filter_3'].'</option>';
			$i++;
			}
			$html .= '</select>';
			
			$html .= '<div>Или по картинке</div>';
			$html .= '<div class="formym_images">';
			$i = 0;
			foreach ($filter_ym as $one_element)
			{
			
			if ($one_element['image']) {
				$image = $this->model_tool_image->resize($one_element['image'], 200, 150);
			} else {
				$image = $this->model_tool_image->resize('no_image.jpg', 200, 150);
			}
			
			$html .= '<div class="formym_element" option_id_select="'.$i.'">';
			$html .= '<img src="'.$image.'" />';
			$html .= '<div>'.$one_element['filter_3'].'</div>';
			$html .= '</div>';
			$i++;
			}
			$html .= '</div>';
		}
		echo $html;
	}
	
	public function getfilterrul() {
		$this->load->model('catalog/product');
		
		$filter_1 = '';
		$filter_2 = '';
		$filter_3 = '';
		if (isset($this->request->get['filter_mark'])) $filter_1 = $this->request->get['filter_mark'];
		if (isset($this->request->get['filter_model'])) $filter_2 = $this->request->get['filter_model'];
		if (isset($this->request->get['filter_ym'])) $filter_3 = $this->request->get['filter_ym'];
		$category_id = $this->request->get['category_id'];
		
		$filter_data = array(
			'filter_category_id' => $category_id,
			'filter_1'	=>	$filter_1,
			'filter_2'	=>	$filter_2,
			'filter_3'	=>	$filter_3
		);
		
		$filter_rul = array();
		if ($filter_3!='')
		{
			$filter_rul = $this->model_catalog_product->getFilterRul($filter_data);
		}
		
		$html = '';
		if ($filter_rul)
		{
			$finded = 0;
			foreach ($filter_rul as $one_element)
			{
			if ($one_element['filter_5']!='')
				$finded=1;
			}
			$html .= '<select name="formrul" filter_mark="'.$filter_1.'" filter_model="'.$filter_2.'" filter_ym="'.$filter_3.'" class="form-control">';
			$html .= '	<option value=""></option>';
			foreach ($filter_rul as $one_element)
			{
			if ($one_element['filter_5']!='')
				$html .= '	<option value="'.$one_element['filter_5'].'">'.$one_element['filter_5'].'</option>';
			}
			$html .= '</select>';
		}
		if ($finded==0) echo "None";
		else echo $html;
	}
	
	public function getfilteroptions() {
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
		$filter_1 = '';
		$filter_2 = '';
		$filter_3 = '';
		$filter_5 = '';
		if (isset($this->request->get['filter_mark'])) $filter_1 = $this->request->get['filter_mark'];
		if (isset($this->request->get['filter_model'])) $filter_2 = $this->request->get['filter_model'];
		if (isset($this->request->get['filter_ym'])) $filter_3 = $this->request->get['filter_ym'];
		if (isset($this->request->get['filter_rul'])) $filter_5 = $this->request->get['filter_rul'];
		$category_id = $this->request->get['category_id'];
		
		$filter_data = array(
			'filter_category_id' => $category_id,
			'filter_1'	=>	$filter_1,
			'filter_2'	=>	$filter_2,
			'filter_3'	=>	$filter_3,
			'filter_5'	=>	$filter_5
		);
		
		$filter_options = $this->model_catalog_product->getFilterOptions($filter_data);
		
		$html = '';
		if ($filter_options)
		{
			$i = 0;
			$html .= '<div class="blik5_options">';
			$filters_array = array();
			foreach ($filter_options as $one_element)
			{
				$filter_array = array();
				$filter_explode = explode(",",$one_element['filter_options']);
				foreach ($filter_explode as $filter_explode_element)
				{
					$filter_explode_razdel = explode("|",$filter_explode_element);
					if ($filter_explode_razdel[0]=='Опции')
					{
						$filter_array[$filter_explode_razdel[1]] = $filter_explode_razdel[2];
					}
				}
				$find = 0;
				foreach ($filters_array as $proverka_array)
				{
					if ($proverka_array==$filter_array)
						$find = 1;
				}
				
				if ($find == 0)
				{
					$filters_array[$i] = $filter_array;
					
					$options = '';
					$for_implode = array();
					foreach ($filter_array as $for_options_key=>$for_options_temp)
					{
						$for_implode[] = implode('|',array('Опции',$for_options_key,$for_options_temp));
					}
					$options = implode(',',$for_implode);
					
					if ($one_element['image']) {
						$image = $this->model_tool_image->resize($one_element['image'], 200, 150);
					} else {
						$image = $this->model_tool_image->resize('no_image.jpg', 200, 150);
					}
					$products_array[$i] = array(
						'name'	=>	$one_element['name'],
						'image'	=>	$image,
						'options'	=>	$options
					);
					$i++;
				}
			}
			
			foreach ($filters_array as $key=>$one_filter)
			{
				$html .= '<div class="blik5_option">';
				$html .= '	<div class="blik5_option_header">'.$products_array[$key]['name'].'</div>';
				$html .= '		<div class="blik5_option_inner">';
				$html .= '			<div class="blik5_option_inner_image"><img src="'.$products_array[$key]['image'].'" class="filter5_select_option_select" filter_mark="'.$filter_1.'" filter_model="'.$filter_2.'" filter_ym="'.$filter_3.'" filter_rul="'.$filter_5.'" filter_options="'.$products_array[$key]['options'].'"></div>';
				$html .= '			<div class="blik5_option_inner_options">';
				foreach ($one_filter as $key_inner=>$value) {
				$html .= '				<div>'.$key_inner.': '.$value.'</div>';
				}
				$html .= '				<div class="filter5_select_option filter5_select_option_select" filter_mark="'.$filter_1.'" filter_model="'.$filter_2.'" filter_ym="'.$filter_3.'" filter_rul="'.$filter_5.'" filter_options="'.$products_array[$key]['options'].'">Выбрать</div>';
				$html .= '			</div>';
				$html .= '		</div>';
				$html .= '</div>';
			}
			
			$html .= '</div>';
		}
		echo $html;
	}
	
	public function getfilterbokovoe() {
		$this->load->model('catalog/product');
		
		$filter_1 = '';
		$filter_2 = '';
		$filter_3 = '';
		if (isset($this->request->get['filter_mark'])) $filter_1 = $this->request->get['filter_mark'];
		if (isset($this->request->get['filter_model'])) $filter_2 = $this->request->get['filter_model'];
		if (isset($this->request->get['filter_ym'])) $filter_3 = $this->request->get['filter_ym'];
		$category_id = $this->request->get['category_id'];
		
		$filter_data = array(
			'filter_category_id' => $category_id,
			'filter_1'	=>	$filter_1,
			'filter_2'	=>	$filter_2,
			'filter_3'	=>	$filter_3
		);
		
		$filter_bokovoe = array();
		if ($filter_3!='')
		{
			$filter_bokovoe = $this->model_catalog_product->getFilterBokovoe($filter_data);
		}
		
		$html = '';
		if ($filter_bokovoe)
		{
			$finded = 0;
			foreach ($filter_bokovoe as $one_element)
			{
			$html .= '<select name="formraspologenie" filter_mark="'.$filter_1.'" filter_model="'.$filter_2.'" filter_ym="'.$filter_3.'" class="form-control">';
			$html .= '	<option value=""></option>';
			foreach ($filter_bokovoe as $one_element)
			{
			if ($one_element['filter_6']!='')
				$html .= '	<option value="'.$one_element['filter_6'].'">'.$one_element['filter_6'].'</option>';
			}
			$html .= '</select>';
			}
		}
		echo $html;
	}
	
	public function getfilterdvigatel() {
		$this->load->model('catalog/product');
		
		$filter_1 = '';
		$filter_2 = '';
		$filter_3 = '';
		if (isset($this->request->get['filter_mark'])) $filter_1 = $this->request->get['filter_mark'];
		if (isset($this->request->get['filter_model'])) $filter_2 = $this->request->get['filter_model'];
		if (isset($this->request->get['filter_ym'])) $filter_3 = $this->request->get['filter_ym'];
		$category_id = $this->request->get['category_id'];
		
		$filter_data = array(
			'filter_category_id' => $category_id,
			'filter_1'	=>	$filter_1,
			'filter_2'	=>	$filter_2,
			'filter_3'	=>	$filter_3
		);
		
		$filter_dvigatel = array();
		if ($filter_3!='')
		{
			$filter_dvigatel = $this->model_catalog_product->getFilterDvigatel($filter_data);
		}
		
		$html = '';
		if ($filter_dvigatel)
		{
			$finded = 0;
			foreach ($filter_dvigatel as $one_element)
			{
			$html .= '<select name="formdvigatel" filter_mark="'.$filter_1.'" filter_model="'.$filter_2.'" filter_ym="'.$filter_3.'" class="form-control">';
			$html .= '	<option value=""></option>';
			foreach ($filter_dvigatel as $one_element)
			{
			if ($one_element['filter_4']!='')
				$html .= '	<option value="'.$one_element['filter_4'].'">'.$one_element['filter_4'].'</option>';
			}
			$html .= '</select>';
			}
		}
		echo $html;
	}
	
	public function getproducts() {
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
		$filter_1 = '';
		$filter_2 = '';
		$filter_3 = '';
		$filter_5 = '';
		$filter_options = '';
		if (isset($this->request->get['filter_mark'])) $filter_1 = $this->request->get['filter_mark'];
		if (isset($this->request->get['filter_model'])) $filter_2 = $this->request->get['filter_model'];
		if (isset($this->request->get['filter_ym'])) $filter_3 = $this->request->get['filter_ym'];
		if (isset($this->request->get['filter_dvigatel'])) $filter_4 = $this->request->get['filter_dvigatel'];
		if (isset($this->request->get['filter_rul'])) $filter_5 = $this->request->get['filter_rul'];
		if (isset($this->request->get['filter_raspologenie'])) $filter_6 = $this->request->get['filter_raspologenie'];
		if (isset($this->request->get['filter_options'])) $filter_options = $this->request->get['filter_options'];
		
		$category_id = $this->request->get['category_id'];
		
		$filter_data = array(
			'filter_category_id' => $category_id,
			'filter_1'	=>	$filter_1,
			'filter_2'	=>	$filter_2,
			'filter_3'	=>	$filter_3,
			'filter_5'	=>	$filter_5,
			'filter_options'	=>	$filter_options
		);
		
		$filter_products = array();
		$filter_products = $this->model_catalog_product->getFilteredProducts($filter_data);
		
		$html = '';
		$html = '<div class="blik5_products_inner">';
		if ($filter_products)
		{
			$i = 0;
			
			foreach ($filter_products as $product)
			{
				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], 200, 150);
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', 200, 150);
				}
				
				$link = $this->url->link('product/product','product_id='.$product['product_id']);
				
				$html .= '<div class="blik5_product">';
				$html .= '	<div class="blik5_product_header"><a href="'.$link.'">'.$product['name'].'</a></div>';
				$html .= '		<div class="blik5_product_inner">';
				$html .= '			<div class="blik5_product_inner_image"><a href="'.$link.'"><img src="'.$image.'" /></a></div>';
				$html .= '			<div class="blik5_product_inner_options">';
				if ($product['manufacturer'])
				$html .= '				<div>Бренд: '.$product['manufacturer'].'</div>';
				$filter_explode = explode(",",$product['filter_options']);
				foreach ($filter_explode as $filter_explode_element)
				{
					$filter_explode_razdel = explode("|",$filter_explode_element);
					if ($filter_explode_razdel[0]=='Параметры')
					{
				$html .= '				<div>'.$filter_explode_razdel[1].': '.$filter_explode_razdel[2].'</div>';
					}
				}
				$html .= '			</div>';
				$html .= '			<div class="blik5_product_inner_right">';
				if ($product['special'])
				$html .= '				<div class="blik5_product_inner_right_price">Цена: <span class="blik5_price_old">'.$this->currency->format($product['price']).'</span><span class="blik5_price_new">'.$this->currency->format($product['special']).'</span></div>';
				else
				$html .= '				<div class="blik5_product_inner_right_price">Цена: <span class="blik5_price_new">'.$this->currency->format($product['price']).'</span></div>';
				$html .= '				<div class="blik5_button_buy button btn-primary button_oc btn" onclick="cart.add(\''.$product['product_id'].'\');">Купить</div>';
				$html .= '			</div>';
				$html .= '		</div>';
				$html .= '</div>';
			}
			
		}
		else
			$html = '<div style="font-weight:bold;font-size:16px;">Товаров с данными параметрами не найдено.</div>';
		
		$html .= '</div>';
		echo $html;
	}
}
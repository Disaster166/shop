<?php
class ModelShippingSkladi extends Model {
	function getQuote($address) {
		$this->load->language('shipping/skladi');

		$method_data = array();
		
		
		$this->load->model('catalog/product');
		
		$products_skladi = $this->cart->getProducts();

		$all_skladis = array();
		$main_skladi = array();
		$skladi_gde_est = array();
		
		$skladi_minus_count = array();
		
		foreach ($products_skladi as $product_skladi) {
			$main_skladi[] = $product_skladi['product_id'];
		}
		
		
		foreach ($products_skladi as $product_skladi) {
			$skladi_minus_count[$product_skladi['product_id']] = $this->model_catalog_product->getSkladisProduct($product_skladi['product_id']);
		}
		
		foreach ($products_skladi as $product_skladi) {
			$inner_array = array();
			foreach ($skladi_minus_count[$product_skladi['product_id']] as $mini_element)
			{
				$inner_array[] = array(
					'name'	=>	$mini_element['name'],
					'quantity'	=>	$mini_element['quantity']-$product_skladi['quantity'],
				);
			}
			$skladi_minus_count[$product_skladi['product_id']] = $inner_array;
		}
		
		foreach ($skladi_minus_count as $one_sklad)
		{
			foreach ($one_sklad as $one_sklad_inner)
			{
				$all_skladis[$one_sklad_inner['name']] = $one_sklad_inner['name'];
			}
		}
		
		$result_array = $all_skladis;
		$main_array = array();
		
		foreach ($skladi_minus_count as $one_sklad)
		{
			foreach ($one_sklad as $one_sklad_inner)
			{
				if ($one_sklad_inner['quantity']<0) $result_array[$one_sklad_inner['name']] = '';
			}
		}
		
		foreach ($result_array as $vivodim=>$test)
		{
			if ($test!='') $main_array[] = $vivodim;
		}
		
		if (!empty($main_array))
		{
			$quote_data = array();
			
			$i = 0;
			foreach ($main_array as $one_elem)
			{
				$quote_data['skladi'.$i] = array(
					'code'         => 'skladi.skladi'.$i,
					'title'        => $one_elem,
					'cost'         => 0.00,
					'tax_class_id' => 0,
					'text'         => $this->currency->format(0.00)
				);
				$i++;
			}

			$method_data = array(
				'code'       => 'skladi',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('skladi_sort_order'),
				'error'      => false
			);
		
			return $method_data;
		}
	}
}
<?php
class ModelShippingSkladi extends Model {
	function getQuote($address) {
		$this->load->language('shipping/skladi');

		$method_data = array();
		
		
		$this->load->model('catalog/product');
		
		// Получаем список товаров
		$products_skladi = $this->cart->getProducts();

		$all_skladis = array();
		$main_skladi = array();
		$skladi_gde_est = array();
		
		$skladi_minus_count = array();
		
		// main_skladi - id всех товаров
		foreach ($products_skladi as $product_skladi) {
			$main_skladi[] = $product_skladi['product_id'];
		}
		
		// skladi_minus_count[id товара] - информация по складам для товара
		foreach ($products_skladi as $product_skladi) {
			$skladi_minus_count[$product_skladi['product_id']] = $this->model_catalog_product->getSkladisProduct($product_skladi['product_id']);
		}
		
		// Отнимаем количество купленного товара и заносим в skladi_minus_count
		foreach ($products_skladi as $product_skladi) {
			$inner_array = array();
			foreach ($skladi_minus_count[$product_skladi['product_id']] as $mini_element)
			{
				$inner_array[] = array(
					'name'	=>	$mini_element['name'],
					'quantity'	=>	$mini_element['quantity']-$product_skladi['quantity'],
					'town_name'	=>	$mini_element['town_name'],
					'address'	=>	$mini_element['address'],
					'worktime'	=>	$mini_element['worktime'],
					'phone'	=>	$mini_element['phone']
				);
			}
			$skladi_minus_count[$product_skladi['product_id']] = $inner_array;
		}
		
		// all_skladis - склад - город
		foreach ($skladi_minus_count as $one_sklad)
		{
			foreach ($one_sklad as $one_sklad_inner)
			{
				$all_skladis[$one_sklad_inner['name']] = array(
					'town_name'	=>	$one_sklad_inner['town_name'],
					'address'	=>	$one_sklad_inner['address'],
					'worktime'	=>	$one_sklad_inner['worktime'],
					'phone'	=>	$one_sklad_inner['phone']
				);
			}
		}
		
		$result_array = $all_skladis;
		$main_array = array();
		
		foreach ($skladi_minus_count as $one_sklad)
		{
			foreach ($one_sklad as $one_sklad_inner)
			{
				if ($one_sklad_inner['quantity']<0) $result_array[$one_sklad_inner['name']] = '';
			}
		}
		
		// Массив для точек самовывоза
		$samovivoz_array = array();
		
		foreach ($result_array as $vivodim=>$test)
		{
			if (is_array($test)) {
				$main_array[] = $test['town_name'];
				$samovivoz_array[] = $test;
			}
		}
		
		// Выбрана доставка по России
		if ( (!isset($this->session->data['current_town'])) || (isset($this->session->data['current_town'])&&$this->session->data['current_town']=='') || (isset($this->session->data['current_town'])&&$this->session->data['current_town']=='Доставка по России') )
		{
			$html_code = '<div>';
			foreach ($samovivoz_array as $one_sklad)
			{
				$html_code .= '<div style="border:1px solid #EEEEEE;padding:3px;"><b>'.$one_sklad['address'].'</b><br />Время работы: '.$one_sklad['worktime'].'<br />Телефон:'.$one_sklad['phone'].'</div>';
			}
			$html_code .= '</div>';
		
			$quote_data['skladisamovivoz'] = array(
				'code'         => 'skladi.skladisamovivoz',
				'title'        => 'Самовывоз'.$html_code,
				'cost'         => 0.00,
				'tax_class_id' => 0,
				'text'         => $this->currency->format(0.00)
			);
			
			$quote_data['skladitk'] = array(
				'code'         => 'skladi.skladitk',
				'title'        => 'Доставка ТК',
				'cost'         => 0.00,
				'tax_class_id' => 0,
				'text'         => $this->currency->format(0.00)
			);

			$method_data = array(
				'code'       => 'skladi',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('skladi_sort_order'),
				'error'      => false
			);
		
		}
		else
		{
			// Есть ли товары в выбранном городе
			$est = 0;
			foreach ($main_array as $one_elem)
			{
				if ($one_elem==$this->session->data['current_town'])
				{
					$est = 1;
				}
			}
			// Товары есть в выбранном городе
			if ($est == 1)
			{
				$quote_data['skladigorod'] = array(
					'code'         => 'skladi.skladigorod',
					'title'        => 'Доставка по городу',
					'cost'         => 0.00,
					'tax_class_id' => 0,
					'text'         => $this->currency->format(0.00)
				);

				$method_data = array(
					'code'       => 'skladi',
					'title'      => $this->language->get('text_title'),
					'quote'      => $quote_data,
					'sort_order' => $this->config->get('skladi_sort_order'),
					'error'      => false
				);
			}
			else
			{
				$quote_data['skladitk'] = array(
					'code'         => 'skladi.skladitk',
					'title'        => 'Доставка ТК',
					'cost'         => 0.00,
					'tax_class_id' => 0,
					'text'         => $this->currency->format(0.00)
				);

				$method_data = array(
					'code'       => 'skladi',
					'title'      => $this->language->get('text_title'),
					'quote'      => $quote_data,
					'sort_order' => $this->config->get('skladi_sort_order'),
					'error'      => false
				);
			}
		}
		
		return $method_data;
	}
}
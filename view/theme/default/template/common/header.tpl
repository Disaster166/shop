<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/javascript/jquery/magnific/magnific-popup.css" type="text/css" rel="stylesheet" media="screen" />
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php echo $google_analytics; ?>
</head>
<body class="<?php echo $class; ?>">

<?php if ($opredelyaem==1) { ?>
<div id="white-popup" class="white-popup mfp-hide">
	<div class="white-popup-header">Не нашли свой город - есть доставка по России</div>
	<div class="white-popup-variants">
		<div class="white-town" town_name="Доставка по России">Доставка по России</div>
	<?php foreach ($towns as $town) { ?>
		<div class="white-town" town_name="<?php echo $town['name']; ?>"><?php echo $town['name']; ?></div>
	<?php } ?>
	</div>
</div>
<script type="text/javascript">
$(function () {
	$.magnificPopup.open({
	  items: {
		src: $('#white-popup'),
		type: 'inline'
	  }
	});
});
</script>
<?php } ?>

<script type="text/javascript">
$(document).ready(function() {
	$(document).delegate('select[name="header_town"]', 'change', function(e) {
		var this_val = $(this).val();
		$.ajax({
			url: 'index.php?route=common/home/settown',
			type: 'get',
			data: 'current_town=' + this_val,
			success: function(html) {
				location.reload();
			}
		});
	});
$('#towns>li>a').on('click', function() {
    var this_val = this.innerHTML;
    $.ajax({
      url: 'index.php?route=common/home/settown',
      type: 'get',
      data: 'current_town=' + this_val,
      success: function(html) {
        location.reload();
      }
    })
    return false;
  });
	$(document).delegate('.white-town', 'click', function(e) {
		var this_val = $(this).attr('town_name');
		$.ajax({
			url: 'index.php?route=common/home/settown',
			type: 'get',
			data: 'current_town=' + this_val,
			success: function(html) {
				location.reload();
			}
		});
	});
$('#header_town>a').on('click', function() {
    $('#towns').slideToggle(100);
return false;
});
});
</script>
<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
    <?php echo $currency; ?>
    <?php echo $language; ?>
	  <ul class="nav navbar-nav">
		 <li><a href="<?php echo $link1; ?>">О нас</a></li>
		 <li><a href="<?php echo $link2; ?>">Доставка</a></li>
		 <li><a href="<?php echo $link3; ?>">Обратная связь</a></li>
	  </ul>
    <div class="nav pull-right">
      <ul class="nav navbar-nav">
        <!--<li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></li>-->
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <!--<li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>-->
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class="container mynav2">
    <div class="row">
      <div class="col-sm-3">
<div class="header_phone"><?php echo $telephone; ?></div>
	  
	<div id="header_town">
		<div class="header_select_town">Город доставки:</div>
    <a href=""><?php echo($current_town)?></a>
	<!--	
     <select name="header_town"  class="form-control">
			<option value=''>Доставка по России</option>
			<?php foreach ($towns as $town) { ?>
			<option value='<?php echo $town['name']; ?>' <?php if ($current_town==$town['name']) { ?>selected="selected"<?php } ?>><?php echo $town['name']; ?></option>
			<?php } ?>
		</select>
    -->

  <ul id="towns">
    <li><a href="">Доставка по России</a></li>
    <?php foreach ($towns as $town) { ?>
    <li><a href=""><?php echo $town['name']; ?></a></li>
    <?php } ?>
  </ul>


	</div>
      </div>

	  <div class="col-sm-6">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-3">
		<?php echo $cart; ?>
		<a class="btn info-btn-class btn-lg btn-block" href="<?php echo $compare_href; ?>"><span id="compare-total">Сравнение товаров (<?php echo $compares; ?>)</span></a>
	  </div>
    </div>
  </div>
</header>
<?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo $home; ?>">Главная</a></li>
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled mynav">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>

        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?>

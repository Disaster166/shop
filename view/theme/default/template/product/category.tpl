<?php echo $header; ?>
<div class="container">
   <?php echo $current_step ?>
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($thumb || $description) { ?>
      <div class="row">
        <?php if ($thumb) { ?>
        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
        <?php if ($description) { ?>
        <div class="col-sm-10"><?php echo $description; ?></div>
        <?php } ?>
      </div>
      <hr>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
	  
	  
		<?php if ($filter_type!=0) { ?>
		<div class="row blik5_filter">

            Выберите марку:
            <span class="filter_select_1">
                <select name="formmark"  class="form-control">
                    <option selected=""></option>
                    <?php foreach ($filter_mark as $element_filter_mark) { ?>
                        <option value="<?php echo $element_filter_mark['filter_1']; ?>" <?php if ($element_filter_mark['filter_1']==$filter_1) echo "selected='selected'";?>><?php echo $element_filter_mark['filter_1']; ?></option>
                    <?php } ?>
                </select>
            </span>

            Выберите модель:
            <span class="filter_select_2">
                <select class="form-control" name="formmodel" filter_mark="<?php echo $filter_1; ?>">
                    <?php if (!empty($filter_model)) { ?>
                    <option selected="" ></option>
                    <?php foreach ($filter_model as $element_filter_model) { ?>
                        <option value="<?php echo $element_filter_model['filter_2']; ?>" <?php if ($element_filter_model['filter_2']==$filter_2) echo "selected='selected'";?>><?php echo $element_filter_model['filter_2']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
            </span>
            <div class="blik5_filter_block">
                <div class="blik5_filter_block_content" <?php if ($current_step>2||$current_step=='showproducts') { echo 'style="display:block;"'; }?>>
                    Выберите модель из списка:
                    <span class="filter_select_3">
                        <?php $i=0; ?>
                        <select class="form-control" name="formym" filter_mark="<?php echo $filter_1; ?>" filter_model="<?php echo $filter_2; ?>">
                            <?php if (!empty($filter_ym)) { ?>
                            <option value="">Все</option>
                            <?php foreach ($filter_ym as $element_filter_ym) { ?>
                                <option  id="option_id_<?php echo $i; ?>" value="<?php echo $element_filter_ym['filter_3']; ?>" filter_mark="<?php echo $filter_1; ?>" filter_model="<?php echo $filter_2; ?>" <?php if ($element_filter_ym['filter_3']==$filter_3) echo "selected='selected'";?>><?php echo $element_filter_ym['filter_3']; ?></option>
                            <?php $i++; } ?>
                            <?php } ?>
                        </select>
                        <div>Или по картинке</div>
                        <div class="row formym_images">
                        <?php
                        $i = 0;
                        foreach ($filter_ym as $one_element)
                        {
                        ?>
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail formym_element" option_id_select="<?php echo $i; ?>">
                                    <img src="<?php echo $one_element['image']; ?>" width="100%" height="100%"/>
                                    <div class="caption"><?php echo $one_element['filter_3']; ?></div>
                                </div>
                            </div>
                        <?php
                        $i++;
                        }
                        ?>
                        </div>
                    </span>
                </div>
            </div>


			<script type="text/javascript">
			$(document).ready(function() {
				<?php if (!$filter_1) { ?>
					$('select[name="formmark"]').val('');
				<?php } ?>
				<?php if (!$filter_2) { ?>
					$('select[name="formmodel"]').val('');
				<?php } ?>
				<?php if (!$filter_3) { ?>
					$('select[name="formym"]').val('');
				<?php } ?>
				<?php if (!$filter_4) { ?>
					$('select[name="formdvigatel"]').val('');
				<?php } ?>
				<?php if (!$filter_5) { ?>
					$('select[name="formrul"]').val('');
				<?php } ?>
				<?php if (!$filter_6) { ?>
					$('select[name="formraspologenie"]').val('');
				<?php } ?>
			
				<?php if ($filter_type!=0&&$current_step == 'showproducts') { ?>
				$("html,body").animate({scrollTop: $('.blik5_products').offset().top}, 400);
				<?php } ?>
				
				<?php if ($current_step!='showproducts') { ?>
					<?php if ($filter_type==1) { ?>	
						<?php if ($filter_5) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_5').offset().top}, 400);
						<?php } else if ($filter_3) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_3').offset().top}, 400);
						<?php } else if ($filter_2) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_3').offset().top}, 400);
						<?php } else if ($filter_1) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_2').offset().top}, 400);
						<?php } ?>
					<?php } ?>
					<?php if ($filter_type==2||$filter_type==6) { ?>	
						<?php if ($filter_2) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_3').offset().top}, 400);
						<?php } else if ($filter_1) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_2').offset().top}, 400);
						<?php } ?>
					<?php } ?>
					<?php if ($filter_type==3) { ?>	
						<?php if ($filter_6) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_4').offset().top}, 400);
						<?php } if ($filter_2) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_3').offset().top}, 400);
						<?php } else if ($filter_1) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_2').offset().top}, 400);
						<?php } ?>
					<?php } ?>
					<?php if ($filter_type==4||$filter_type==5) { ?>	
						<?php if ($filter_4) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_4').offset().top}, 400);
						<?php } if ($filter_2) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_3').offset().top}, 400);
						<?php } else if ($filter_1) { ?>
							//$("html,body").animate({scrollTop: $('.blik5_filter_block_2').offset().top}, 400);
						<?php } ?>
					<?php } ?>
				<?php } ?>
				
				$(document).delegate('.blik5_filter_block_header', 'click', function(e) {
					var current_url = $(this).attr('current_url');
					if (current_url)
					location = current_url;
				});
			
				$(document).delegate('.formym_element', 'click', function(e) {
					var option_id_select = $(this).attr('option_id_select');
					$('#option_id_'+option_id_select).prop('selected', true);
					$('select[name="formym"]').change();
				});
			
				$(document).delegate('select[name="formmark"]', 'change', function(e) {
					var filter_mark = $(this).val();
					location = '<?php echo $current_url; ?>&filter_mark='+filter_mark;
				});
				
				
				$(document).delegate('select[name="formmodel"]', 'change', function(e) {
					var filter_mark = $(this).attr('filter_mark');
					var filter_model = $(this).val();
					location = '<?php echo $current_url; ?>&filter_mark='+filter_mark+'&filter_model='+filter_model;
				});
			});	
			</script>
				
			<?php if ($filter_type==1) { ?>	
			<script type="text/javascript">
			$(document).ready(function() {
				$(document).delegate('select[name="formym"]', 'change', function(e) {
					var filter_mark = $(this).attr('filter_mark');
					var filter_model = $(this).attr('filter_model');
					var filter_ym = $(this).val();
					location = '<?php echo $current_url; ?>&filter_mark=' + filter_mark+'&filter_model=' + filter_model+'&filter_ym=' + filter_ym;
				});
				
				
				$(document).delegate('select[name="formrul"]', 'change', function(e) {
					var filter_mark = $(this).attr('filter_mark');
					var filter_model = $(this).attr('filter_model');
					var filter_ym = $(this).attr('filter_ym');
					var filter_rul = $(this).val();
					location = '<?php echo $current_url; ?>&filter_mark=' + filter_mark+'&filter_model=' + filter_model+'&filter_ym=' + filter_ym+'&filter_rul='+filter_rul;
				});
				
				$(document).delegate('.filter5_select_option_select', 'click', function(e) {
					
					var filter_mark = $(this).attr('filter_mark');
					var filter_model = $(this).attr('filter_model');
					var filter_ym = $(this).attr('filter_ym');
					var filter_rul = $(this).attr('filter_rul');
					var filter_options = $(this).attr('filter_options');
					$('.blik5_option').removeClass('selected');
					$(this).parent().parent().parent().addClass('selected');
					location = '<?php echo $current_url; ?>&filter_mark='+filter_mark+'&filter_model='+filter_model+'&filter_ym='+filter_ym+'&filter_rul='+filter_rul+'&filter_options='+filter_options;
				});
				
			});
			</script>
			<?php } ?>
			
			<?php if ($filter_type==1) { ?>
			<?php if (($current_step>4||$current_step=='showproducts')&&!$filter_5) { ?>
			<?php } else { ?>
			<div class="blik5_filter_block">

				<div class="blik5_filter_block_content" <?php if ($current_step>3||$current_step=='showproducts') echo 'style="display:block;"'; ?>>
					Расположение руля:
					<span class="filter_select_4">
						<select class="form-control" name="formrul" filter_mark="<?php echo $filter_1; ?>" filter_model="<?php echo $filter_2; ?>" filter_ym="<?php echo $filter_3; ?>">
							<?php if (!empty($filter_rul)) { ?>
							<option selected="" ></option>
							<?php foreach ($filter_rul as $element_filter_rul) { ?>
								<option value="<?php echo $element_filter_rul['filter_5']; ?>" <?php if ($element_filter_rul['filter_5']==$filter_5) echo "selected='selected'";?>><?php echo $element_filter_rul['filter_5']; ?></option>
							<?php } ?>
							<?php } ?>
						</select>
					</span>
				</div>
			</div>
			<?php } ?>
			<div class="blik5_filter_block">
				<div class="blik5_filter_block_content" <?php if ($current_step>4||$current_step=='showproducts') echo 'style="display:block;"'; ?>>
					<span class="filter_select_5">
					    <h1>Опции:</h1>
						<div class="row blik5_options">
						<?php
							foreach ($filters_array as $key=>$one_filter)
							{
						?>      <div class="col-sm-6 col-md-4">
                                    <div class="thumbnail blik5_option <?php if ($products_array[$key]['options']==$filter_options) echo "selected"; ?>">

                                            <img src="<?php echo $products_array[$key]['image']; ?>"  width="100%" height="100%" class="filter5_select_option_select" filter_mark="<?php echo $filter_1; ?>" filter_model="<?php echo $filter_2; ?>" filter_ym="<?php echo $filter_3; ?>" filter_rul="<?php echo $filter_5; ?>" filter_options="<?php echo $products_array[$key]['options']; ?>">
                                            <div class="caption blik5_option_inner_options">
                                                <?php foreach ($one_filter as $key_inner=>$value) { ?>
                                                    <div><?php echo $key_inner; ?>: <?php echo $value; ?></div>
                                                <?php } ?>
                                                <div class="filter5_select_option filter5_select_option_select" filter_mark="<?php echo $filter_1; ?>" filter_model="<?php echo $filter_2; ?>" filter_ym="<?php echo $filter_3; ?>" filter_rul="<?php echo $filter_5; ?>" filter_options="<?php echo $products_array[$key]['options']; ?>">Выбрать</div>
                                            </div>

                                    </div>
                                </div>
						<?php
							}
						?>
						</div>
					
					</span>
				</div>
			</div>
			<?php } ?>
			
			<?php if ($filter_type==2||$filter_type==6) { ?>	
			<?php } ?>
			
			<?php if ($filter_type==2||$filter_type==6) { ?>	
			<script type="text/javascript">
			$(document).ready(function() {
				
				$(document).delegate('select[name="formym"]', 'change', function(e) {
					var filter_mark = $(this).attr('filter_mark');
					var filter_model = $(this).attr('filter_model');
					var filter_ym = $(this).val();
					location = '<?php echo $current_url; ?>&filter_mark='+filter_mark+'&filter_model='+filter_model+'&filter_ym='+filter_ym;
				});
				
			});
			</script>
			<?php } ?>
			
			<?php if ($filter_type==3) { ?>	
			<div class="blik5_filter_block">
				<div class="blik5_filter_block_content" <?php if ($current_step>3||$current_step=='showproducts') echo 'style="display:block;"'; ?>>
					Выберите расположение:
					<span class="filter_select_4">
						<select class="form-control" name="formraspologenie" filter_mark="<?php echo $filter_1; ?>" filter_model="<?php echo $filter_2; ?>" filter_ym="<?php echo $filter_3; ?>">
							<?php if (!empty($filter_dvigatel)) { ?>
							<option selected="" ></option>
							<?php foreach ($filter_raspologenie as $element_filter_raspologenie) { ?>
								<option value="<?php echo $element_filter_raspologenie['filter_6']; ?>" <?php if ($element_filter_raspologenie['filter_6']==$filter_6) echo "selected='selected'";?>><?php echo $element_filter_raspologenie['filter_6']; ?></option>
							<?php } ?>
							<?php } ?>
						</select>
					</span>
				</div>
			</div>
			<?php } ?>
			
			<?php if ($filter_type==3) { ?>	
			<script type="text/javascript">
			$(document).ready(function() {
			
				$(document).delegate('select[name="formym"]', 'change', function(e) {
					var filter_mark = $(this).attr('filter_mark');
					var filter_model = $(this).attr('filter_model');
					var filter_ym = $(this).val();
					location = '<?php echo $current_url; ?>&filter_mark=' + filter_mark+'&filter_model=' + filter_model+'&filter_ym=' + filter_ym;
				});
				
				$(document).delegate('select[name="formraspologenie"]', 'change', function(e) {
					var filter_mark = $(this).attr('filter_mark');
					var filter_model = $(this).attr('filter_model');
					var filter_ym = $(this).attr('filter_ym');
					var filter_raspologenie = $(this).val();
					location = '<?php echo $current_url; ?>&filter_mark='+filter_mark+'&filter_model='+filter_model+'&filter_ym='+filter_ym+'&filter_raspologenie='+filter_raspologenie;
				});
				
			});
			</script>
			<?php } ?>
			
			<?php if ($filter_type==4||$filter_type==5) { ?>	
			<div class="blik5_filter_block">
				<div class="blik5_filter_block_content" <?php if ($current_step>3||$current_step=='showproducts') echo 'style="display:block;"'; ?>>
					Выберите двигатель:
					<span class="filter_select_4">
						<select class="form-control" name="formdvigatel" filter_mark="<?php echo $filter_1; ?>" filter_model="<?php echo $filter_2; ?>" filter_ym="<?php echo $filter_3; ?>">
							<?php if (!empty($filter_dvigatel)) { ?>
							<option selected="" ></option>
							<?php foreach ($filter_dvigatel as $element_filter_dvigatel) { ?>
								<option value="<?php echo $element_filter_dvigatel['filter_4']; ?>" <?php if ($element_filter_dvigatel['filter_4']==$filter_4) echo "selected='selected'";?>><?php echo $element_filter_dvigatel['filter_4']; ?></option>
							<?php } ?>
							<?php } ?>
						</select>
					</span>
				</div>
			</div>
			<?php } ?>
			
			<?php if ($filter_type==4||$filter_type==5) { ?>	
			<script type="text/javascript">
			$(document).ready(function() {
			
				$(document).delegate('select[name="formym"]', 'change', function(e) {
					var filter_mark = $(this).attr('filter_mark');
					var filter_model = $(this).attr('filter_model');
					var filter_ym = $(this).val();
					location = '<?php echo $current_url; ?>&filter_mark='+filter_mark+'&filter_model='+filter_model+'&filter_ym='+filter_ym;
				});
				
				$(document).delegate('select[name="formdvigatel"]', 'change', function(e) {
					var filter_mark = $(this).attr('filter_mark');
					var filter_model = $(this).attr('filter_model');
					var filter_ym = $(this).attr('filter_ym');
					var filter_dvigatel = $(this).val();
					location = '<?php echo $current_url; ?>&filter_mark='+filter_mark+'&filter_model='+filter_model+'&filter_ym='+filter_ym+'&filter_dvigatel='+filter_dvigatel;
				});
				
			});
			</script>
			<?php } ?>
			
		</div>
		
		
		
		<?php } ?>
	  
	  
	<?php if ($current_step=='showproducts') { ?>
	<div class="blik5_products">
	<div class="blik5_products_inner">
			
	<?php		
		$i=0;
		foreach ($products as $product)
			{
		?>		
				<div class="blik5_product">
					<div class="blik5_product_header"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
					<div class="blik5_product_inner">
						<div class="blik5_product_inner_image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" /></a></div>
						<div class="blik5_product_inner_options">
						<?php if ($product['manufacturer']) { ?>
							<div>Бренд: <?php echo $product['manufacturer']; ?></div>
						<?php } ?>
							<?php
							$filter_explode = explode(",",$product['filter_options']);
							foreach ($filter_explode as $filter_explode_element)
							{
								$filter_explode_razdel = explode("|",$filter_explode_element);
								if ($filter_explode_razdel[0]=='Параметры')
								{ ?>
									<div><?php echo $filter_explode_razdel[1]; ?>: <?php echo $filter_explode_razdel[2]; ?></div>
								<?php 
								}
							}
							?>
						</div>
						<div class="blik5_product_inner_right">
						<?php if ($customer_group==4) { ?>
						<?php if ($product['special']) { ?>
							<div class="blik5_product_inner_right_price">Цена: <span class="blik5_price_old"><?php echo $product['price']; ?></span><span class="blik5_price_new"><?php echo $product['special']; ?></span></div>
						<?php } else { ?>
							<div class="blik5_product_inner_right_price">Цена: <span class="blik5_price_new"><?php echo $product['price']; ?></span></div>
							<div class="blik5_product_inner_right_price">Цена оптовая: <span class="blik5_price_new"><?php echo $product['price_2']; ?></span></div>
							<div class="blik5_product_inner_right_price">Цена установки: <span class="blik5_price_new"><?php echo $product['price_3']; ?></span></div>
						<?php } ?>
						<?php } ?>
							<div class="blik5_div_buttons">
							<div class="blik5_button_buy button btn-primary button_oc btn" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i><span class="addtocart_span">В корзину</span></div>
							<button type="button" id="compare-button" class="btn btn-default" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i><span> В сравнение</span></button>
							</div>
						</div>
					</div>

				<?php if (($customer_group==4) && (count($warehouses[$i])>0)) { ?>
				<div class="blik5_product_warehouse"><span>Наличие на складах <?php echo($_SESSION['current_town'])?>a:</span>
					<table class="warehouse">
					<tr>
					<?php 
					for ($i; $i<count($warehouses); $i++) {
						
						for ($j=0; $j<count($warehouses[$i]); $j++)  {

							echo("<th data-toggle='tooltip' data-html='true' title='".$warehouses[$i][$j]['address']."'>".$warehouses[$i][$j]['name_short']."</th>");
						
						}

						break;

					}
					?>
					</tr>
					<tr>
					<?php 
					for ($i; $i<count($warehouses); $i++) {

						for ($j=0; $j<count($warehouses[$i]); $j++)  {

							echo("<td>".$warehouses[$i][$j]['quantity']."</td>");

						}

						break;

					}

					$i++;
					?>
					</tr>
					</table>

				</div>
				<?php } ?>
			</div>
			<?php } ?>
			
	</div>
	</div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
		<?php } ?>
	  
      <?php if ($products&&0) { ?>
	  
      <!--<div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-2 text-right">
          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
        </div>
        <div class="col-md-3 text-right">
          <select id="input-sort" class="form-control" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-md-1 text-right">
          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
        </div>
        <div class="col-md-2 text-right">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
      </div>
      <br />-->
      <div class="row">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-list col-xs-12">
			<div class="product-list-header"><?php echo $product['name']; ?></div>
			<div class="product-list-body">
				<table>
					<tr>
						<th class="col-list-1"></th>
						<th class="col-list-2">Цвет</th>
						<th class="col-list-3">Бренд</th>
						<?php if ($customer_group==4) { ?><th class="col-list-4">Цена</th><?php } ?>
						<th class="col-list-5"></th>
						<th class="col-list-6"></th>
						<th class="col-list-7"></th>
					</tr>
					<tr>
						<td class="col-list-1" rowspan="<?php echo 1+count($product['analogs']);?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></td>
						<td class="col-list-2"><a href="<?php echo $product['href']; ?>"><?php echo $product['color']; ?></a></td>
						<td class="col-list-3"><a href="<?php echo $product['href']; ?>"><?php echo $product['manufacturer']; ?></a></td>
						<?php if ($customer_group==4) { ?><td class="col-list-4"><a href="<?php echo $product['href']; ?>"><?php echo $product['price']; ?></a></td><?php } ?>
						<td class="col-list-5"><a href="<?php echo $product['href']; ?>">подробнее</a></td>
						<td class="col-list-6"><a class="button btn-primary button_oc btn" onclick="cart.add('<?php echo $product['product_id']; ?>');"><span>В корзину</span></a></td>
						<td class="col-list-7"><a class="button btn-primary button_oc btn" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span>В закладки</span></a></td>
					</tr>
					<?php if ($product['analogs']) { ?>
						<?php foreach ($product['analogs'] as $analog) { ?>
							<tr>
								<td class="col-list-2"><a href="<?php echo $analog['href']; ?>"><?php echo $analog['color']; ?></a></td>
								<td class="col-list-3"><a href="<?php echo $analog['href']; ?>"><?php echo $analog['manufacturer']; ?></a></td>
								<?php if ($customer_group==4) { ?><td class="col-list-4"><a href="<?php echo $analog['href']; ?>"><?php echo $analog['price']; ?></a></td><?php } ?>
								<td class="col-list-5"><a href="<?php echo $analog['href']; ?>">подробнее</a></td>
								<td class="col-list-6"><a class="button btn-primary button_oc btn" onclick="cart.add('<?php echo $analog['product_id']; ?>');"><span>В корзину</span></a></td>
								<td class="col-list-7"><a class="button btn-primary button_oc btn" onclick="wishlist.add('<?php echo $analog['product_id']; ?>');"><span>В закладки</span></a></td>
							</tr>
						<?php } ?>
					<?php } ?>
				</table>
			</div>
		</div>
        <!--<div class="product-layout product-list col-xs-12">
          <div class="product-thumb">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div>
              <div class="caption">
                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                <p><?php echo $product['description']; ?></p>
                <?php if ($product['rating']) { ?>
                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <?php } ?>
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                  <?php if ($product['tax']) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
              </div>
              <div class="button-group">
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
              </div>
            </div>
          </div>
        </div>-->
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>

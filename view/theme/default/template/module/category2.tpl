<div class="list-group">
  <?php foreach ($categories as $category2) { ?>
  <?php if ($category2['category_id'] == $category_id) { ?>
  <a href="<?php echo $category2['href']; ?>" class="list-group-item active"><?php echo $category2['name']; ?></a>
  <?php if ($category2['children']) { ?>
  <?php foreach ($category2['children'] as $child) { ?>
  <?php if ($child['category_id'] == $child_id) { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } else { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } ?>
  <?php } ?>
  <?php } ?>
  <?php } else { ?>
  <a href="<?php echo $category2['href']; ?>" class="list-group-item"><?php echo $category2['name']; ?></a>
  <?php } ?>
  <?php } ?>
</div>
